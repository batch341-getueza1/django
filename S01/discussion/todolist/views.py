from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.forms.models import model_to_dict
# from django.template import loader

# Local imports
from .models import ToDoItem

# Create your views here.
def index(request):
    todoitem_list = ToDoItem.objects.all()
    # output = ', '.join([todoitem.task_name for todoitem in todoitem_list])
    # template = loader.get_template("todolist/index.html")
    # context = {'todoitem_list' : todoitem_list}
    # return HttpResponse(template.render(context, request))

    # Creates a context dictionary
    context = { 
        'todoitem_list': todoitem_list,
        'user': request.user

    }
    return render(request, "todolist/index.html", context)

def todoitem(request, todoitem_id):
    # response = "You are viewing the details of %s"
    # return HttpResponse(response % todoitem_id)
    # The model_to_dict() allows to convert models into dictionaries
    # get() allows to retrieve a record using it primary key(pk)
    todoitem = model_to_dict(ToDoItem.objects.get(pk=todoitem_id))
    return render(request, "todolist/todoitem.html", todoitem)

def register(request):
    users = User.objects.all()
    is_user_registered = False
    context = {
        "is_user_registered": is_user_registered
    }

    for indiv_user in users:
        if indiv_user.username == "johndoe":
            is_user_registered = True
            break

    if is_user_registered == False:
        user = User()
        user.username = "johndoe"
        user.first_name = "John"
        user.last_name = "Doe"
        user.email = "johndoe@mail.com"
        # set_password() is used to ensure that the password is hashed using Django's authenticaiton framework
        user.set_password("john1234")
        user.is_staff = False
        user.is_active = True
        user.save()
        context = {
            "first_name" : user.first_name,
            "last_name" : user.last_name
        }
    return render(request, "todolist/register.html", context)

def change_password(request):
    is_user_authenticated = False

    # authenticates the user
    # returns the user object if found and "None" if not found
    user = authenticate(username="johndoe", password="john1234")
    print(user)

    if user is not None:
        authenticate_user = User.objects.get(username="johndoe")
        authenticate_user.set_password("john1")
        authenticate_user.save()
        is_user_authenticated = True

    context = {
    "is_user_authenticated": is_user_authenticated
    }
    return render(request, "todolist/change_password.html", context)

def login_view(request):
    username = "johndoe"
    password = "john1"
    user = authenticate(username=username, password=password)
    context = {
    "is_user_authenticated": False
    }
    print(user)
    if user is not None:
        # Saves creates a record in the django_session table in the database
        login(request, user)
        # redirects the user to the index.html page
        return redirect("index")
    else:
        return render(request, "todolist/login.html", context) 

def logout_view(request):
    logout(request)
    return redirect("index")