from django.contrib import admin
from django.urls import include, path

# Every django project always starts with the package that has the same name. In order for us to access the routes of a different package (todolist), then we have to import/include it within the 'urls.py' file within the package that has the same name as the project.
urlpatterns = [
    # By doing this, we are going to be able to access the routes from the urls.py of the todolist package.
    path('todolist/', include('todolist.urls')),
    path('admin/', admin.site.urls),
]