from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout

# Local imports
from .models import GroceryItem

# Create your views here.
def index(request):
	groceryitem_list = GroceryItem.objects.all()
	context = { 'groceryitem_list': groceryitem_list}
	return render(request, 'django_practice/index.html', context)

def groceryitem(request, groceryitem_id):
	response = "You are viewing the details of %s"
	return HttpResponse(response % groceryitem_id)

def register(request):
	users = User.objects.all()
	is_user_registered = False
	context = {
		"is_user_registered": is_user_registered
	}

	for indiv_user in users:
		if indiv_user.username == "johndoe":
			is_user_registered = True
			break

	if is_user_registered == False:
		user = User()
		user.username = "johndoe"
		user.first_name = "John"
		user.last_name = "Doe"
		user.email = "johndoe@mail.com"
		user.set_password("john1234")
		user.is_staff = False
		user.is_active = True
		user.save()
		context = {
			"first_name" : user.first_name,
			"last_name" : user.last_name
		}
	return render(request, "django_practice/register.html", context)

def change_password(request):
	is_user_authenticated= False

	user = authenticate(username="johndoe", password="john1234")
	print(user)

	if user is not None:
		authenticate_user = User.objects.get(username="johndoe")
		authenticate_user.set_password("john1")
		authenticate_user.save()
		is_user_authenticated = True
	
	context = {
		"is_user_authenticated": is_user_authenticated
	}
	return render(request, "django_practice/change_password.html", context)

def login_view(request):
	username = "johndoe"
	password = "john1"
	user = authenticate(username=username, password=password)
	context = {
		"is_user_authenticated": False
	}
	print(user)
	if user is not None:
		login(request, user)
		return redirect("index")
	else:
		return render(request, "django_practice/login.html", context)

def logout_view(request):
	logout(request)
	return redirect("index")
